# Changelog
# v0.21.1-restic-v0.12.0
Fixed some unescaped `test` commands in entrypoint.

# v0.21.0-restic-v0.12.0
Added cleanup to restic cache after repository got pruned.

# v0.13.0-restic-v0.11.0
Removed `--json` from global command.
# v0.12.0-restic-v0.11.0
Removed `--verbose=3` from global command.
# v0.11.0-restic-v0.11.0
Changed logging from custom json to simple echo.

# v0.10.0-restic-v0.11.0
Added RESTIC_BACKUP_ARGS, RESTIC_CHECK_ARGS and RESTIC_POLICY environment variables to kubernetes cronjob.

# v0.9.0-restic-v0.11.0
Removed forget and check from restic modes, this schould run manually or
in another automatic process.

# v0.8.0-restic-v0.11.0
Using spec.nodeName with fileRef to get the RESTIC_HOST var initialized.

# v0.7.0-restic-v0.11.0
Removed --read-data-subset=1/256 from RESTIC_CHECK_ARGS.
Updated README.md

# v0.6.0-restic-v0.11.0
Added specific labels to metadata section of objects.
Moved common labels to commonLables generator in kustomization.yml.
Removed backup-data emptyDir from volumes spec.
Added examples.

# v0.5.1-restic-v0.11.0
Added changelog for this version.
Removed volumeMounts and volumes.
Should be only added by users.
Added persistentVolumeClaim configuration to ensure that cache is persisted.
Removed --no-cache from RESTIC_GLOBAL_ARGS.

# v0.4.0-restic-v0.11.0
Added pvc for restic cache files.

# v0.1.0-restic-v0.11.0
Version bumped restic base image.

# v0.1.0-restic-v0.10.0

Initial release
