FROM restic/restic:0.12.0

ENV LOG_LEVEL="INFO"
ENV RESTIC_GLOBAL_ARGS=""
ENV RESTIC_CACHE_DIR="/restic/cache"
ENV RESTIC_MODES="backup"
ENV RESTIC_REPOSITORY=""
ENV RESTIC_HOST=""
ENV RESTIC_PASSWORD=""
ENV RESTIC_BACKUP_ARGS=""
ENV RESTIC_CHECK_ARGS=""
ENV RESTIC_FORGET_ARGS=""

RUN apk add --update --no-cache openssh-client && \
  mkdir -p /restic/cache

COPY entrypoint.sh /usr/bin/entrypoint.sh
ENTRYPOINT [ "/bin/sh", "-c", "/usr/bin/entrypoint.sh" ]
