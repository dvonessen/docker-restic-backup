# restic-backup

This Docker Image is inspired by [lobaro/restic-backup-docker](https://github.com/lobaro/restic-backup-docker)
So feel free to checkout their repository as well.

Thanks to [lobaro](https://github.com/lobaro).

## Usage

To use this image, you have to bind mount your directory to backup to `/data` choose the `backup` mode and specify `RESTIC_REPOSITORY` and `RESTIC_PASSWORD`.

## Variables
|       Variable       | Default              |     Mandatory     | Description                                                                                                                                                               |
| :------------------: | :------------------- | :---------------: | :------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
|     `LOG_LEVEL`      | "INFO"               |                   | Setting the logging level of the entrypoint. Not restic itself, see `RESTIC_GLOBAL_ARGS`. Possible values: `DEBUG`, `INFO`                                                |
| `RESTIC_GLOBAL_ARGS` | "--verbose=1 --json" |                   | Restic global arguments. `restic --help`                                                                                                                                  |
|    `RESTIC_MODE`     | "backup"             |                   | Modes which are executed. Possible modes: `backup`, `check`, `forget`. You can use all modes together in a space delimited string. Modes are executed in the given order. |
| `RESTIC_REPOSITORY`  | ""                   |         x         | Repository to backup to. If repository does not exists it will be created. This ensures, that backups created.                                                            |
|  `RESTIC_PASSWORD`   | ""                   |         x         | Repository password which is used to encrypt the backup.                                                                                                                  |
|    `RESTIC_HOST`     | ""                   |         x         | Hostname or identifier to use to distinct the backup from other hosts.                                                                                                    |
| `RESTIC_BACKUP_ARGS` | ""                   |                   | Restic backup arguments. `restic backup --help`.                                                                                                                          |
| `RESTIC_CHECK_ARGS`  | ""                   |                   | Restic check arguments. Defaults to use a subset of data to read, that prevents massive reads on the repository but ensures backups integrity `restic check --help`.      |
| `RESTIC_FORGET_ARGS` | ""                   | If mode `forget`. | If you want to forget backups add a policy. If you use mode `forget` it will prune old backups as well. `restic forget --help`.                                           |
| `RESTIC_PRUNE_ARGS`  | ""                   | If mode `prune`.  | You can specify the behavior of the `prune` command.`restic prune --help`.                                                                                                |
