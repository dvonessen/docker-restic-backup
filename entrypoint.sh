#!/bin/sh

if [ -n "$DEBUG" ]
then
    set -x
fi

RESTIC="$(command -v restic) ${RESTIC_GLOBAL_ARGS}"
if [ -z "${RESTIC}" ]; then
    echo "ERROR: restic command not found."
    exit 127
fi

cleanup() {
    $RESTIC unlock --remove-all
    rc=$?
    if [ $rc == 0 ]
    then
        echo "INFO: Unlocking repository successfull. Restic return code: $rc"
    else
        echo "ERROR: Unlocking repository unsuccessfull."
    fi
}
trap cleanup EXIT

echo "INFO: Sanity checking mandatory variables."
if [ -z "${RESTIC_MODES}" ]
then
    echo "ERROR: Environment variable RESTIC_MODES not set."
    exit 127
elif [ -z "${RESTIC_REPOSITORY}" ]
then
    echo "ERROR: Environment variable RESTIC_REPOSITORY not set."
    exit 127
elif [ -z "${RESTIC_PASSWORD}" ]
then
    echo "ERROR: Environment variable RESTIC_PASSWORD not set."
    exit 127
elif [ -z "${RESTIC_HOST}" ]
then
    echo "ERROR: Environment variable RESTIC_HOST not set."
    exit 127
fi

echo "INFO: Starting backup process..."
echo "INFO: Check if repository exist..., if not creating repository on ${RESTIC_REPOSITORY}"
$RESTIC init
rc=$?
if [ $rc == 1 ]
then
    if [ -n ${rc_msg##*config file already exists*} ]
    then
        echo "INFO: Repository ${RESTIC_REPOSITORY} already exists. Proceeding with backup."
    else
        echo "ERROR: Error while creating new repository: ${RESTIC_REPOSITORY}. Restic return code: $rc"
        exit 127
    fi
else
    echo "INFO: Repository ${RESTIC_REPOSITORY} did not exists, hence it was cerated."
fi

for restic_mode in $RESTIC_MODES
do
    case $restic_mode in
        "backup")
            test -z "${RESTIC_HOST}" && "ERROR: Variable 'RESTIC_HOST' is empty. Please add a backup host." && exit 1
            echo "INFO: Create backup..."
            echo "INFO: $RESTIC backup --host ${RESTIC_HOST} ${RESTIC_BACKUP_ARGS} /data"
            $RESTIC backup --host ${RESTIC_HOST} ${RESTIC_BACKUP_ARGS} /data
            rc=$?
            if [ $rc == 0 ]
            then
                echo "INFO: Backup successfull."
                echo "INFO: Available snapshots."
                echo "INFO: $RESTIC snapshots"
                $RESTIC snapshots
            else
                echo "ERROR: Backup unsuccessfull! Restic return code: $rc"
                exit 127
            fi
        ;;
        "check")
            echo "INFO: Check backup..."
            echo "INFO: $RESTIC check ${RESTIC_CHECK_ARGS}"
            $RESTIC check ${RESTIC_CHECK_ARGS}
            rc=$?
            if [ $rc == 0 ]
            then
                echo "INFO: Backup check successfull."
            else
                echo "ERROR: Backup check unsuccessfull. Restic return code: $rc"
                exit 127
            fi
        ;;
        "forget")
            test -z "${RESTIC_FORGET_ARGS} abs" && "ERROR: RESTIC_FORGET_ARGS missing."
            echo "INFO: Forget old backups."
            echo "INFO: $RESTIC forget --host ${RESTIC_HOST} ${RESTIC_FORGET_ARGS}"
            $RESTIC forget --host ${RESTIC_HOST} ${RESTIC_FORGET_ARGS}
            rc=$?
            if [ $rc == 0 ]
            then
                echo "INFO: Forget check successfull."
            else
                echo "ERROR: Forget check unsuccessfull. Restic return code: $rc"
                exit 127
            fi
        ;;
        "prune")
            echo "INFO: Prune old backups."
            echo "INFO: $RESTIC prune ${RESTIC_PRUNE_ARGS}"
            $RESTIC prune ${RESTIC_PRUNE_ARGS}
            test -n "$RESTIC_CACHE_DIR" && rm -rf $RESTIC_CACHE_DIR/*
            rc=$?
            if [ $rc == 0 ]
            then
                echo "INFO: Prune successfull."
            else
                echo "ERROR: Prune unsuccessfull. Restic return code: $rc"
                exit 127
            fi
        ;;
        *)
            echo "ERROR: RESTIC_MODE: ${restic_mode} not available. Available modes are backup|check|forget."
            exit 1
    esac
done
